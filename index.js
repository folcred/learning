// Get our module and the prompt library imported for use.
const Person = require('./person');
const prompt = require('prompt-sync')({sigint:true});

// Create the variable holders for our input.
let name, age, job, hobby, year, make, model, color;

// Prompt the user to enter the information.
name = prompt('Enter your name: ');
age = prompt('Enter your age: ');
job = prompt('Enter your occupation: ');
hobby = prompt('Enter your favorite hobby: ');
year = prompt('Enter the manuafacture year of your car: ');
make = prompt('Enter the make of your car: ');
model = prompt('Enter the model of your car: ');
color = prompt('Enter the color of your car: ')

// Use our Person module to populate the class objects.
const person1 = new Person(name, age, job, hobby, year, make, model, color);

// Call the module functions to display the result.
person1.greeting();
person1.forFun();
person1.auto();
person1.likes();

// Object oriented programming

// Using a constructor function
function Person(firstName, lastName, dob) {
  this.firstName = firstName;
  this.lastName = lastName;
  // Using the Date method we can get an actual date
  this.dob = new Date(dob);
  this.getBirthYear = function () {
    return this.dob.getFullYear();
  }
  this.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
  }
}

// Instantiate object
const person1 = new Person('John', 'Doe', '4-3-1980');
const person2 = new Person('Mary', 'Smith', '3-10-1977');

// We can see how the Date methods work
// First with using the method directly
console.log(person1.firstName, person1.dob.getFullYear());
// Then with using our method created in the constructor
console.log(person2.getFullName(), person2.getBirthYear());

// Sometimes it's desirable to use prototypes instead
// of creating everything in the constructor
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}

Car.prototype.getFullInfo = function () {
  return `${this.year} ${this.make} ${this.model}`;
}

car1 = new Car('Ford', 'Taurus', '1996');
console.log(car1.getFullInfo());

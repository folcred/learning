// This is about working with file systems

const fs = require('fs');
const path = require('path');

// Create a folder
/*
fs.mkdir(path.join(__dirname, '/test'), {}, err => {
  if (err) throw err;
  console.log('Folder created...');
});
*/

// Create and write to a file
/*
fs.writeFile(path.join(__dirname, '/test', 'hello.txt'), 'Hello there!', err => {
  if (err) throw err;
  console.log('File written to...');
    // To add to the file in the same function we need to place it in the callback
    // otherwise this would be by itself, which would be more real world use
      fs.appendFile(path.join(__dirname, '/test', 'hello.txt'), 'Learning Javascript.', err => {
          if (err) throw err;
          console.log('File appended to...');
      }
  );
});
*/
// Reading from a file
/*
fs.readFile(path.join(__dirname, '/test', 'hello.txt'), 'utf8', (err, data) => {
  if (err) throw err;
  console.log(data);
});
*/

// Rename a file
fs.rename(path.join(__dirname, '/test', 'hello.txt'),
  path.join(__dirname, '/test', 'hellojs.txt'),
  err => {
    if (err) throw err;
    console.log('File renamed...');
  }
);

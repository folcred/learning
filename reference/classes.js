/*  OOP using newer Javascript classes
    These are basically the same as using
    functional programming, they just tend
    to be easier to create, and are more
    familiar to people coming from other
    languages.
*/

class Car {
  constructor(make, model, year) {
    this.make = make;
    this.model = model;
    this.year = year;
  }
  getFullInfo() {
    return `${this.year} ${this.make} ${this.model}`;
  }
}

const car1 = new Car('Ford', 'Taurus', '1998');

console.log(car1.getFullInfo());

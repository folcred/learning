// Creating an object array
const todos = [
  {
    id: 1,
    text: 'Take out trash',
    isCompleted: true
  },
  {
    id: 2,
    text: 'Meet with boss',
    isCompleted: true
  },
  {
    id: 3,
    text: 'Visit Mom',
    isCompleted: false
  }
];

console.log(todos[1].text);

// An array can be converted to JSON format for export
const todoJSON = JSON.stringify(todos);
console.log(todoJSON);

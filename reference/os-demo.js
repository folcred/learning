// Working with operating systems

const os = require('os');

// Checking the platform
console.log(os.platform());

// CPU Architecture
console.log(os.arch());

// CPU Core Info (this creates an array of information)
// console.log(os.cpus())

// Free memory
console.log(os.freemem() / 1024);

// Total memory
console.log(os.totalmem() / 1024);

// Home folder
console.log(os.homedir());

// Uptime (I added the part to calculate down to decimal days)
console.log((os.uptime() / ((60 * 60) * 24)).toFixed(1));

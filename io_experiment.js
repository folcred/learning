/*
  This is a file I/O experiment. It takes the values from an array and parses
  them into a file, one item per line. The "for" loop breaks the array into
  individual items. Each item is then passed to the 'fs.appendFileSync' routine
  which, if the file doesn't exist, creates it and adds the items to it, if
  it does exist, it appends each item to it. The '\n' added to the words makes
  sure each is put on it's own line in the file.
*/

// This pulls in some Javascript modules we need, and sets our variables.
newFunction();
function newFunction() {
    const fs = require("fs");
    const words = new Array('raspberries', 'apples', 'oranges', 'pears', 'grapes', 'melons');

    // This is just an exercise in adding, changing, and removing some of the array words.
    words.push('mangoes');
    words.unshift('Sunshine');
    words.splice(3, 1);

    sortedWords = words.sort();

    /*
      This is where all the action takes place. The important thing here is
      we need to get the Linux filesystem file descriptor number. These file
      methods do NOT close the file automatically. So what happens is, the file
      is read again from disk, -without- the newly added text from this append.
      Worse, if the file doesn't already exist, it throws an error.
      In order to make sure the file exists -before- we try to read back what
      we just wrote in, we need to make sure the file is closed first, hence
      we need the file descriptor to do it.
    */
    let fd;
    for (let i = 0; i < sortedWords.length; i++) {
        putMe = sortedWords[i] + '\n';
        fd = fs.openSync('./reference/test/array.txt', 'a'); // Need to get the file descriptor.
        fs.appendFileSync(fd, putMe, 'utf8');
    };

    if (fd !== undefined)
        fs.closeSync(fd); // File has to be closed for re-reading later.

    /*
      This next one shows one way of reading a file back into an array. I still have a lot
      to learn about I/O operations and how to work with files, but this is a start.
      One thing I had to ensure was the script having enough time to write the file to
      disk before trying to read it. So I stuck a little prompt in to give time.
    */
    let newFile = fs.readFileSync('./reference/test/array.txt', 'utf8');
    let textByLine = newFile.split('\n');
    for (let n = 0; n < textByLine.length; n++) {
        console.log(textByLine[n]);
    };
}


/*
 A basic algebra program that takes a problem from the user where "x" is
 an unknown value, and processes it to find the actual number. What is
 hard in it is not having any spaces or other delimiters to slice the
 formula and find where x is, so we have to not only find x, but where
 everything else is, and how large they are, how many numbers are part
 of the formula.
 */

const prompt = require('prompt-sync')({ siginit: true });
let problem = prompt("Enter the formula to solve for: ");

/*
We need to make sure they didn't enter any spaces in the formula. Then
we check to be sure they entered an "x" to solve for. If either test
fails, a message is displayed to the user and the program exits.
*/

if (problem.includes(" ")) {
    console.log("You must NOT use spaces in the formula! Try again.");
    process.exit();
}

if (problem.includes("x")) {
    console.log("Calculating...")
} else {
    console.log("You must use 'x' as the problem to search for! Try again.");
    process.exit();
}

/*
Now let's see if we can find x and friends. First we need
to disect the problem, then find where everything that isn't
a number is.

To start we look for where in the string x and the = sign are,
and set those positions to immutable numbers to use later.
*/

const xloc = Number(problem.indexOf("x"));
const equals = Number(problem.indexOf("="));

/*
Then look for where and what the operation is. We need to set the
operator location to a position number, and set what type it is.
TODO: There's surely a better way to do this.
*/

if ((problem.indexOf("+")) != -1) {
    oper = Number(problem.indexOf("+"));
    optype = "+";
} else if ((problem.indexOf("-")) != -1) {
    oper = Number(problem.indexOf("-"));
    optype = "-";
} else if ((problem.indexOf("*")) != -1) {
    oper = Number(problem.indexOf("*"));
    optype = "*";
} else {
    oper = Number(problem.indexOf("/"));
    optype = "/";
}

/*
Using the information we already have, now we break out the numbers.
If x isn't the first position, then it's a number, if x is first,
then between the operator and equals is the first number. Once we
find it, we store it in the variable "firstNum".
*/

if (xloc != 0) {
    firstNum = Number(problem.substring(0, oper));
} else {
    firstNum = Number(problem.substring(oper + 1, equals));
}

/*
To find the 2nd number, if x is last, the 2nd number is between operator
and the equals sign. Otherwise it's in the last place. Once found we store
it in the variable "secondNum".
*/

if (xloc == Number(problem.length - 1)) {
    secondNum = Number(problem.substring(oper + 1, equals));
} else {
    secondNum = Number(problem.substring(equals + 1,));
}

/*
Here we need to turn the position of x into a marker number that will
be used in later logic to know which formula to use. Whether x is in the
1st position, middle, or last, decides what formula to use for solving.
*/

if (xloc == 0) {
    position = 0;
} else if ((xloc > oper) && (xloc < equals)) {
    position = 2;
} else {
    position = 4;
}

/*
Now that we have it disected, and everything assigned, we need some formulas
to do the math. We'll make some functions for this and pass in the variables.
*/

function addNums(firstNum, secondNum) {
    return firstNum + secondNum;
}

function subNums(firstNum, secondNum) {
    return firstNum - secondNum;
}

function multiNums(firstNum, secondNum) {
    return firstNum * secondNum;
}

function divNums(firstNum, secondNum) {
    return firstNum / secondNum;
}

/*
Here we pull it all together, testing positions to know which formula to use,
and outputting the result back to the user.
*/

if ((optype == "+" && position == 0) || (optype == "+" && position == 2)) {
    console.log("x equals", subNums(secondNum, firstNum));
} else if (optype == "+" && position == 4) {
    console.log("x equals", addNums(firstNum, secondNum));
} else if (optype == "-" && position == 0) {
    console.log("x equals", addNums(secondNum, firstNum));
} else if ((optype == "-" && position == 2) || (optype == "-" && position == 4)) {
    console.log("x equals", subNums(firstNum, secondNum));
} else if ((optype == "*" && position == 0) || (optype == "*" && position == 2)) {
    console.log("x equals", divNums(secondNum, firstNum));
} else if (optype == "*" && position == 4) {
    console.log("x equals", multiNums(firstNum, secondNum));
} else if (optype == "/" && position == 0) {
    console.log("x equals", multiNums(secondNum, firstNum));
} else if ((optype == "/" && position == 2) || (optype == "/" && position == 4)) {
    console.log("x equals", divNums(firstNum, secondNum));
} else {
    console.log("You must have entered something wrong!");
}

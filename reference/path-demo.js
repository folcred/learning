const path = require('path');

// Getting the base filename
console.log('The current file name is: ', path.basename(__filename));

// Getting the folder name
console.log('The current path is: ', path.dirname(__filename));

// Getting the file extension
console.log('The current file extension is: ', path.extname(__filename));

// Create a file object
console.log('This is a file information object: ', path.parse(__filename));

// Return part of the path object
console.log('This is the filename of that object: ', path.parse(__filename).base);

// Concatenate paths (or create new ones)
console.log('This is a new file path: ', path.join(__dirname, 'test', 'hello.js'));

// Showing the differences in data types

const x = 10;

// The "==" checks for equal values, ignoring data types
if (x == 10) {
  console.log('x is equal to 10');
}

// The "===" checks for equal values, including data types
if (x === 10) {
  console.log('x is equal to 10');
}

const y = 20;

if (y === 10) {
  console.log('y is 10');
} else if (y > 10) {
  console.log('y is greater than 10');
} else {
  console.log('y is less than 10');
}

// Logic checks
// One condition needs to match to be true
if (x > 5 || y < 10) {
  console.log('x is more than 5 OR y is less than 10');
}

// Both conditions need to match to be true
if (x > 5 && y > 10) {
  console.log('x is more than 5 AND y is less than 10');
}

// Ternary oprator "?" if true first value, ":" if false, second value
const z = 12;

const color = z > 10 ? 'RED' : 'BLUE';
console.log(`The color is ${color}.`);

// Using switch to compare data
switch (color) {
  case 'RED':
    console.log('Color is red');
    break;
  case 'BLUE':
    console.log('Color is Blue');
    break;
  default:
    console.log('Don\'t know what color it is');
    break;
}

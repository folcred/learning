/*
 The outer function creates records passed into it
 while the inner functions, the closures, decide who
 can make changes to those records. There are two
 inner functions, one that allows anyone to change
 the address, the other that checks whether the person
 changing the name has the requisite permission to
 do so, if not, no change is made.
*/

let resourceRecord = function(myName, myAddress) {
  let resourceName = myName;
  let resourceAddress = myAddress;
  let accessRight = "HR";
  return {
    changeName: function(updateName, privilege) {
      // only those in HR can change names
      if(privilege === accessRight) {
        resourceName = updateName;
        return true;
      } else {
        return false;
      }
    },
    changeAddress: function(newAddress) {
      // anyone can change addresses
      resourceAddress = newAddress;
    },
    showResourceDetail: function() {
      console.log("Name:" + resourceName + " ; Address:" + resourceAddress);
    }
  }
}

let resourceRecord1 = resourceRecord("Perry", "Office");
let resourceRecord2 = resourceRecord("Emma", "Office");

resourceRecord1.showResourceDetail();
resourceRecord2.showResourceDetail();

resourceRecord1.changeAddress("Home"); // This works due to being an address
resourceRecord1.changeName("Perry Berry", "Associate"); // This does not
resourceRecord2.changeName("Emma Freeman", "HR"); // This does as has right privilege

resourceRecord1.showResourceDetail();
resourceRecord2.showResourceDetail();

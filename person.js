/*
This module creates a Person class with default fields in it.
It also provides some functions for output of the data that was
entered by a user.
*/

class Person {
  constructor(name, age, job, hobby, year, make, model, color) {
    this.name = name;
    this.age = age;
    this.job = job;
    this.hobby = hobby;
    this.year = year;
    this.make = make;
    this.model = model;
    this.color = color;
  }

  greeting() {
    console.log(`My name is ${this.name} and I am ${this.age} years old and work as a ${this.job}.`);
  }

  forFun() {
    console.log(`For fun I like to do ${this.hobby}.`);
  }

  auto() {
    console.log(`My car is a ${this.color} ${this.year} ${this.make} ${this.model}.`);
  }

  likes() {
    console.log(`There's nothing I like more than ${this.hobby} and my ${this.make} ${this.model}!`);
  }
}

module.exports = Person;

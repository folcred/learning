/*This is an exercise I did to try and work on my own
  rather than copy someone else's code. It's a standard
  learning program, but I've tried to create the whole
  thing by myself, to LEARN to code and solve my own
  problems.
*/

// First we get a guess from the user.
const prompt = require('prompt-sync')({sigint: true});
console.log('\nYou can quit at any time by pressing \"Ctrl+c\"');
let guess = prompt(`Enter a guess from 1 to 100: `);

// This creates a random number to be guessed.
let random = Math.random() * 100;
let guessMe = Math.round(random);
let count = 0;
let guesses = ""

/*
  Now we play the game. We match what the player entered
  against the random number, if lower or higher they're told
  which and try again.
  We also need to check that they actually enter a number,
  if not, they're reminded to do so. We keep a count of how
  many times it took to guess the number, displaying it
  at the end of the game.
*/
while (guess != guessMe) {
  count = count + 1;
  tries = 10 - count;
  guesses += guess;
  guesses += ' ';
  if (guess < guessMe) {
    console.log(`\nSo far you've guessed ${guesses}\n`)
    guess = prompt(`You guessed too low, you have ${tries} guesses left, guess again: `);
  } else if (guess > guessMe) {
      console.log(`\nSo far you've guessed ${guesses}\n`)
      guess = prompt(`You guessed too high, you have ${tries} left, guess again: `);
  // If they didn't enter a valid number, try again.
  } else if (isNaN(guess)) {
    console.log(`\nSo far you've guessed ${guesses}\n`)
    guess = prompt(`You must enter a number between 1 and 100. You have ${tries} left. Try again: `);
  };
}

console.log(`You got it in ${count} tries!`);

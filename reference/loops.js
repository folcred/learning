// Loops and various uses

// For loops
// for(let i = 0 ; i <= 10; i++) {
//   console.log(`For loop Number: ${i}`);
// }

// While loops
// let i = 0;
// while(i < 10) {
//   console.log(`While loop Number: ${i}`);
//   i++; // Important to have incrementing, otherwise will endless loop
// }

const todos = [
  {
    id: 1,
    text: 'Take out trash',
    isCompleted: true
  },
  {
    id: 2,
    text: 'Meet with boss',
    isCompleted: true
  },
  {
    id: 3,
    text: 'Visit Mom',
    isCompleted: false
  }
];

// Using looping to get values from an array
for (let todo of todos) {
  console.log(todo.text);
}

// Using "forEach" to cycle through and array
todos.forEach(function (todo) {
  console.log(todo.text);
});

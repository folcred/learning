/* Temperature converter
    First takes input from the user on what they want to convert
    from, then asks for the value they want to convert. It takes
    the first input to know which conversion to apply, then takes
    the second input and does the conversion on it.

    The "siginit" in the prompt is code in the node module
    "prompt-sync" which allows the user to exit the program
    with Ctrl-c if they decide they don't want to run it to end.
*/

const prompt = require('prompt-sync')({sigint: true});
const temp = prompt("Convert to Fahrenheit 'f' or Celcius 'c' ?: ");
let input = 0;

function toCelcius(fahrenheit) {
  return (5 / 9) * (fahrenheit - 32)
};

function toFahrenheit(celcius) {
  return (celcius * 9 / 5) + 32
};

if (temp == "f") {
  input = prompt("What temperature to convert? : ");
  console.log(`${input} Celcius is ${toFahrenheit(input).toFixed(1)} degrees Fahrenheit`);
} else if (temp == "c") {
  input = prompt("What temperature to convert? : ");
  console.log(`${input} Fahrenheit is ${toCelcius(input).toFixed(1)} degrees Celcius`);
} else {
  console.log(`Sorry, can't convert ${temp}. You have to enter either 'c' or 'f'.`);
}

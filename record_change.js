/*
  Project: Create a group of names and hobbies, then create a function
  that asks the user what they want to change. The user will need the
  right permission to enact the change.
  The program starts by printing out the current list of people, asks
  the user which one to change. Once selected, it asks what change to
  make. It checks if user has permission by prompting for password,
  if not, produces error message alerting the user. Finally, prints out
  the current state of the person name and hobby.
*/

const prompt = require('prompt-sync')({sigint: true});

let Person = function(Name, Hobby) {
  return {
    changeName: function(updateName) {
        Name = updateName;
        return true;
    },
    changeHobby: function(updateHobby) {
        Hobby = updateHobby;
        return true;
    },
    showDetails: function() {
      console.log("Name: " + Name + ", Hobby: " + Hobby);
    }
  }
}

let Person1 = Person("John", "Fishing");
let Person2 = Person("Pete", "Gaming");
let Person3 = Person("Sally", "Painting");

Person1.showDetails();
Person2.showDetails();
Person3.showDetails();

let checkMe = prompt("Enter password: ");
if (checkMe != "password") {
  console.log("Wrong password! You do not have permission!")
  process.exit();
}

let whichPerson = prompt("Which record to edit?: ");
let whatField = prompt("Edit name[1] or hobby[2]?: ");
let newValue = prompt("Enter new value: ");
let beSure = prompt("Are you sure you want to do this? (y/n): ");

if(beSure == "n") {
  process.exit();
}

/*
  TODO There should be a way to enter a person number and concatenate
  it to the Person, otherwise it would be very redundant to do all
  the if blocks with a large dataset. Perhaps a switch statement would
  do this better? Maybe a function?
*/
if(whichPerson == 1 && whatField == 1) {
  Person1.changeName(newValue);
  Person1.showDetails();
} else if(whichPerson == 1 && whatField == 2) {
  Person1.changeHobby(newValue);
  Person1.showDetails();
} else if(whichPerson == 2 && whatField == 1) {
  Person2.changeName(newValue);
  Person2.showDetails();
} else if(whichPerson == 2 && whatField == 2) {
  Person2.changeHobby(newValue);
  Person2.showDetails();
} else if(whichPerson == 3 && whatField == 1) {
  Person3.changeName(newValue);
  Person3.showDetails();
} else if(whichPerson == 3 && whatField == 2) {
  Person3.changeHobby(newValue);
  Person3.showDetails();
} else {
  console.log("You must enter valid data!")
}

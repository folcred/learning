/*This is a common exercise that I thought I'd try to do
  myself in Javascript. It asks the user to enter a number
  for how many levels they want in the tree. Then it
  creates the tree using spaces and hash marks.
*/

const prompt = require('prompt-sync')({sigint: true});
let tree_height = prompt("How tall is the tree? ");
let spaces = tree_height - 1;
let hashes = 1;
const stump = tree_height -1;
const space = " ";
const hash = "#";

while (tree_height > 0) {
  console.log(`${space.repeat(spaces)} ${hash.repeat(hashes)}`);
  spaces -= 1;
  hashes += 2;
  tree_height -= 1;
};
console.log(`${space.repeat(stump)} #`);

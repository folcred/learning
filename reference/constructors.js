// Constructors to create objects
const person = {
  firstName: 'John',
  lastName: 'Doe',
  hobbies: ['music', 'movies', 'sports'],
  address: {
    street: '50 Main St.',
    city: 'Boston',
    state: 'MA'
  },
  personal: {
    age: 30,
    weight: 205
  },
};

console.log(person.firstName, person.lastName);
console.log(person.hobbies[1]);
console.log(person.address.city, person.address.state);

// Destructuring
const { firstName, lastName, address: { city }, address: {state} } = person;

// Output using concatenation
console.log(firstName, lastName, city + ",", state);

// Output using template strings instead of concatenating
console.log(`My name is ${person.firstName} and I'm ${person.age} years old.`);

// Properties can be added to an object
person.email = 'john@email.com';
console.log(person);

person.personal.weight = "210";

// Template strings are a great way to get exactly the information wanted
console.log(`My name is ${person.firstName} ${person.lastName}, I'm ${person.personal.age} and weigh ${person.personal.weight} lbs.`);
console.log(`My name is ${person.firstName} ${person.lastName}, and I live at ${person.address.street} in ${person.address.city}`);

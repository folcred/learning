// Arrays - variables that hold multiple values

// There are a couple ways to create arrays, one is with a constructor
const numbers = new Array(1, 2, 3, 4, 5, 6);
// console.log(numbers);

// The other is to directly create the array
const fruits = ['apples', 'oranges', 'pears'];
// console.log(fruits)

fruits[3] = 'grapes'; // Adds to the array when length is known
// console.log(fruits)

fruits.push('mangoes'); // Adds to the end regardless of length
// console.log(fruits)

fruits.unshift('strawberries'); // Adds to the beginning
// console.log(fruits);

fruits.pop(); // When no index given, removes last item from the array
// console.log(fruits)

// console.log(Array.isArray('hello')); // Test if is an array
// console.log(fruits.indexOf('oranges')); // Returns where item is in array
// console.log(fruits); // Outputs contents of array

// Create an array from items in a string or other input
const words = 'apples, oranges, pears, grapes, melons';
console.log(words.split(', ')); // Splits words at the ", " (comma and space) into array

// Creating an object array
const todos = [
  {
    id: 1,
    text: 'Take out trash',
    isCompleted: true
  },
  {
    id: 2,
    text: 'Meet with boss',
    isCompleted: true
  },
  {
    id: 3,
    text: 'Visit Mom',
    isCompleted: false
  }
];

console.log(`Todo: \"${todos[1].text}\" is done \"${todos[1].isCompleted}\"`);

// Using "forEach" to get values from array
todos.forEach(function (todo) {
  console.log(todo.text);
});

// Using map method to get values from an array into another array
const todoText = todos.map(function (todo) {
  return todo.text;
});

console.log(todoText);

// Filter for values in an array and create a new array from them
const todoCompleted = todos.filter(function (todo) {
  return todo.isCompleted === true;
  // Add a map to return only the text
}).map(function (todo) {
  return todo.text;
});

console.log(`${todoCompleted}`);

const todoNotDone = todos.filter(function (todo) {
  return todo.isCompleted === false;
}).map(function (todo) {
  return todo.text;
});

console.log(`Not done! ${todoNotDone}`);

#!/bin/node
// Figure out the average from input of a series of numbers

const prompt = require('prompt-sync')({sigint: true});
let total = 0;
let count = 0;

// Get the numbers from the user.
let numbers = prompt(`Enter numbers separated by space :`);

// Strip out all the spaces.
let Parsing = numbers.split(" ");

// Then convert them to an array of intergers.
let input = Parsing.map(Number);

// Do the math here.
function addUp () {
  for(let i = 0; i < input.length; i++) {
    count += 1;
    num = input[i];
    total += num;
  };
  avg = total / count;
  return avg
};

console.log('The average is %i: ', addUp());

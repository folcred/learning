// Functions can have default values
function addNums(num1 = 1, num2 = 1) {
  return num1 + num2;
}

// If just called with no parameters, will output defaults
console.log(addNums());

// Defaults can be overridden when the function is called
console.log(addNums(5, 5));

/*
    An arrow function is where a function is turned into a const variable.
    It reduces having to have a "return" or other means of showing the
    result of the function within the function itself.
*/
const subNums = (num1 = 2, num2 = 1) => num1 - num2;

console.log(subNums());
console.log(subNums(9, 5));

// If the arrow function only takes one parameter, the "()" aren't needed
const divNums = num3 => num3 / 2;

console.log(divNums(6));

// Converts between inches and centimeters

const prompt = require('prompt-sync')({sigint: true});
const which = prompt("Convert to centimeters 'c' or inches 'i'? : ");
let input = 0

function toCentimeters(inches) {
  return inches / 0.39370
};

function toInches(centimeters) {
  return centimeters * 0.39370
};


switch(which) {
  case "c":
    input = prompt("What distance to convert? :");
    console.log(`${input} inches is ${toCentimeters(input).toFixed(2)} centimeters.`);
  break;
  case "i":
    input = prompt("What distance to convert? :");
    console.log(`${input} centimeters is ${toInches(input).toFixed(2)} inches.`);
  break;
  default:
    console.log(`Sorry, can't convert ${which}.`)
}
